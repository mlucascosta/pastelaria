# PASTELARIA

projeto de pastelaria utilizado para teste de laravel

## Instruções

- copiar os arquivo ```.env.example``` na pasta principal do projeto e criar um ```.env```
- copiar os arquivo ```.env.example``` na pasta backend/app do projeto e criar um ```.env```

## Instalador

adicionar permissão para o arquivo ```instalar.sh``` para que ele possa criar todo o processo de
subir o docker e isntalar o ambiente de desenvolvimento.