<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProdutoController;
use App\Http\Controllers\PedidoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('clientes')->group(function () {
    Route::get('', [ClienteController::class, 'index']);
    Route::get('{id}', [ClienteController::class, 'show']);
    Route::post('', [ClienteController::class, 'store']);
    Route::put('{id}', [ClienteController::class, 'update']);
    Route::delete('{id}', [ClienteController::class, 'delete']);
    Route::delete('{id}/force', [ClienteController::class, 'destroy']);
});

Route::prefix('produtos')->group(function () {
    Route::get('', [ProdutoController::class, 'index']);
    Route::get('{id}', [ProdutoController::class, 'show']);
    Route::post('', [ProdutoController::class, 'store']);
    Route::put('{id}', [ProdutoController::class, 'update']);
    Route::delete('{id}', [ProdutoController::class, 'delete']);
    Route::delete('{id}/force', [ProdutoController::class, 'destroy']);
});

Route::prefix('pedidos')->group(function () {
    Route::get('', [PedidoController::class, 'index']);
    Route::get('{id}', [PedidoController::class, 'show']);
    Route::get('/cliente/{id}', [PedidoController::class, 'showByCliente']);
    Route::post('', [PedidoController::class, 'store']);
    Route::put('{id}', [PedidoController::class, 'update']);
    Route::delete('{id}', [PedidoController::class, 'delete']);
    Route::delete('{id}/force', [PedidoController::class, 'destroy']);
});
