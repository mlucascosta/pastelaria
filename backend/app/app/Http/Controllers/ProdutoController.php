<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Produto;
use App\Http\Requests\CreateProdutoRequest;

class ProdutoController extends Controller
{
    public function index()
    {
        $produtos = Produto::all();

        foreach ($produtos as $produto) {
            $produto->foto = asset(Storage::url($produto->foto));
        }

        return response()->json($produtos);
    }

    public function store(CreateProdutoRequest $request)
    {
        $produto = new Produto;
        $produto->nome = $request->nome;
        $produto->preco = $request->preco;

        if ($request->hasFile('foto')) {
            $nomeFoto = $this->salvarFoto($request->file('foto'));
            $produto->foto = $nomeFoto;
        }

        $produto->save();

        return response()->json($produto, 201);
    }

    public function show($id)
    {
        $produto = Produto::find($id);

        if ($produto) {
            $produto->foto = asset(Storage::url($produto->foto));
            return response()->json($produto);
        }

        return response()->json(['message' => 'Produto não encontrado'], 404);
    }

    public function update(Request $request, $id)
    {
        $produto = Produto::findOrFail($id);

        $produto->nome = $request->nome;
        $produto->preco = $request->preco;

        if ($request->hasFile('foto')) {
            Storage::delete($produto->foto);
            $nomeFoto = $this->salvarFoto($request->file('foto'));
            $produto->foto = $nomeFoto;
        }
        $produto->save();
        return response()->json($produto);
    }

    public function delete($id)
    {
        $cliente = Produto::findOrFail($id);
        $cliente->delete();
        return response()->json(['message' => 'Registro excluído com sucesso.']);
    }

    public function destroy($id)
    {
        $produto = Produto::withTrashed()->find($id);

        Storage::delete(App(Produto::class)->foto);
        $produto->delete();
        return response()->json(['message' => 'Produto removido com sucesso']);
    }

    private function salvarFoto($foto)
    {
        $nomeFoto = preg_replace('/[^A-Za-z0-9\.]/', '', $foto->getClientOriginalName());
        $caminhoFoto = $foto->storeAs('public/produtos', $nomeFoto);
        return str_replace('public/', '', $caminhoFoto);
    }
}