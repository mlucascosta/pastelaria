<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;
use App\Http\Requests\CreateClienteRequest;
use Carbon\Carbon;

class ClienteController extends Controller
{
    public function index()
    {        
        $clientes = App(Cliente::class)->all();
        return response()->json($clientes);
    }

    public function show($id)
    {
        $cliente = App(Cliente::class)->findOrFail($id);
        return response()->json($cliente);
    }

    public function store(CreateClienteRequest $request)
    {
        $cliente = new Cliente();
        $dados = [
            'nome' => $request->nome,
            'email' => $request->email,
            'telefone' => $request->telefone,
            'data_nascimento' => Carbon::parse($request->data_nascimento)->format('Y-m-d'),
            'endereco' => $request->endereco,
            'complemento' => $request->complemento,
            'bairro' => $request->bairro,
            'cep' => $request->cep,
        ];
        $cliente->fill($dados);
        $cliente->save();
        return response()->json($cliente, 201);
    }

    public function update(CreateClienteRequest $request, $id)
    {
        $cliente = Cliente::findOrFail($id);        
        $cliente->fill($request->all());
        $cliente->save();
        return response()->json($cliente, 200);
    }

    public function delete($id)
    {
        $cliente = App(Cliente::class)->findOrFail($id);
        $cliente->delete();
        return response()->json(['message' => 'Registro excluído com sucesso.']);
    }

    public function destroy($id)
    {
        $pedido = App(Cliente::class)->withTrashed()->find($id);
        $pedido->forceDelete();
        return response()->json(['message' => 'Registro excluído com sucesso.']);
    }
}
