<?php

namespace App\Http\Controllers;

use App\Mail\PedidoRealizado;
use App\Models\Pedido;
use App\Models\PedidoItem;
use App\Http\Requests\CreatePedidoRequest;
use App\Http\Requests\PedidoItemRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PedidoController extends Controller
{
    public function index()
    {
        $pedidos = Pedido::all();
        return response()->json($pedidos);
    }

    public function show($id)
    {
        $pedido = PedidoItem::where(['pedido_id' => $id])->get();
        return response()->json($pedido);
    }

    public function showByCliente($cliente_id)
    {
        $pedido = Pedido::where(['cliente_id' => $cliente_id])->get();
        foreach($pedido as $item):
            $dados[] = PedidoItem::where(['pedido_id' => $item->id])->get();
        endforeach;
        return response()->json($dados);
    }

    public function store(Request $request)
    {
        $pedido = Pedido::create(['cliente_id' => $request->input('cliente_id')]);
        $data = [
            'pedido_id' => $pedido->id,
            $request->input('pedidoItem')
        ];
        $items = new PedidoItem();
        $items->fill($data);
        $items->save();
        Mail::to($pedido->cliente->email)->send(new PedidoRealizado($pedido));
        return response()->json($pedido, 201);
    }

    public function update(Request $request, $id)
    {
        dd($request);
        $pedido = Pedido::findOrFail($id);
        $pedido->cliente_id = $request->input('cliente_id');
        $data = [
            'pedido_id' => $pedido->id,
            $request->input('pedidoItem')
        ];
        $items = new PedidoItem();
        $items->fill($data);
        $items->save();
        Mail::to($pedido->email_cliente)->send(new PedidoRealizado($pedido));
        return response()->json($pedido, 200);
    }

    public function delete($id)
    {
        $cliente = Pedido::findOrFail($id);
        $cliente->delete();
        return response()->json(['message' => 'Registro excluído com sucesso.']);
    }

    public function destroy($id)
    {
        $pedido = Pedido::withTrashed()->find($id);        
        $pedido->forceDelete();
        return response()->json(['message' => 'Registro excluído com sucesso.']);
    }
}
