<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'nome' => 'required',
            'email' => 'required|email|unique:clientes,email',
            'telefone' => 'required',
            'data_nascimento' => 'required',
            'endereco' => 'required',
            'complemento' => 'nullable',
            'bairro' => 'required',
            'cep' => 'required|regex:/^[0-9]{5}-[0-9]{3}$/',
        ];
    }

    public function messages(): array
    {
        return [
            'nome.required' => 'O campo nome é obrigatório.',
            'email.required' => 'O campo email é obrigatório.',
            'email.email' => 'O campo email deve ser um endereço de email válido.',
            'email.unique' => 'Já existe um cliente cadastrado com esse endereço de email.',
            'telefone.required' => 'O campo telefone é obrigatório.',
            'data_nascimento.required' => 'O campo data de nascimento é obrigatório.',
            'data_nascimento.date' => 'O campo data de nascimento deve ser uma data válida.',
            'endereco.required' => 'O campo endereço é obrigatório.',
            'bairro.required' => 'O campo bairro é obrigatório.',
            'cep.required' => 'O campo CEP é obrigatório.',
            'cep.regex' => 'O campo CEP deve estar no formato 00000-000.'
        ];
    }
}
