<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PedidoItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'quantidade' => 'required|integer|min:1',
            'produto_id' => 'required|integer|exists:produtos,id',
        ];
    }

    public function messages(): array
{
    return [
        'quantidade.required' => 'A quantidade é obrigatória',
        'quantidade.integer' => 'A quantidade deve ser um número inteiro',
        'quantidade.min' => 'A quantidade deve ser pelo menos 1',
        'produto_id.required' => 'O ID do produto é obrigatório',
        'produto_id.integer' => 'O ID do produto deve ser um número inteiro',
        'produto_id.exists' => 'O ID do produto deve ser existente na tabela de produtos',
    ];
}

}
