<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends Model
{
    use HasFactory, SoftDeletes;
    public $table = 'pedidos';

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function produto()
    {
        return $this->belongsTo(Produto::class);
    }

    public function PedidoItem()
    {
        return $this->hasMany(PedidoItem::class);
    }

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'cliente_id'
    ];
}
