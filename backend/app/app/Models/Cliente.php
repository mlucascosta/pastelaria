<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use HasFactory, SoftDeletes;
    public $table = 'clientes';

    protected $dates = ['deleted_at'];

    public function pedidos()
    {
        return $this->hasMany(Pedido::class);
    }

    protected $fillable = [
        'nome', 
        'email', 
        'telefone', 
        'data_nascimento', 
        'endereco', 
        'complemento', 
        'bairro', 
        'cep', 
        'data_cadastro'
    ];
}
