<p>Olá {{ $pedido->cliente->nome }},</p>

<p>O seu pedido foi realizado com sucesso.</p>

<h4>Detalhes do pedido:</h4>

<ul>
    @foreach ($pedido->produtos as $produto)
        <li>{{ $produto->nome }} - R$ {{ $produto->preco }}</li>
    @endforeach
</ul>

