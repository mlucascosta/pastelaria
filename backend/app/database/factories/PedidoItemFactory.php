<?php

namespace Database\Factories;

use App\Models\Pedido;
use App\Models\PedidoItem;
use App\Models\Produto;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Cliente>
 */
class PedidoItemFactory extends Factory
{
    protected $model = PedidoItem::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'pedido_id' => Pedido::inRandomOrder()->first(),
            'produto_id' => Produto::inRandomOrder()->first(),
            'quantidade' => rand(1, 10),
        ];
    }
}
