<?php

namespace Database\Seeders;

use App\Models\PedidoItem;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PedidoItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PedidoItem::factory()->count(100)->create();
    }
}
