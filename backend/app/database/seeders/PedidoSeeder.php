<?php

namespace Database\Seeders;

use App\Models\Pedido;
use App\Models\Cliente;
use App\Models\Produto;
use App\Models\PedidoItem;
use Illuminate\Database\Seeder;

class PedidoSeeder extends Seeder
{
    public function run()
    {

        Pedido::factory()->count(50)->create();
    }
}
