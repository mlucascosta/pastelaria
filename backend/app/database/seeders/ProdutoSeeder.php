<?php

namespace Database\Seeders;

use App\Models\Produto;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;


class ProdutoSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        for ($i = 1; $i <= 10; $i++) {
            $produto = new Produto();
            $produto->nome = "Pastel de {$i}";
            $produto->preco = rand(10, 100);
            $produto->foto = $this->salvarFoto($faker->slug, $faker->imageUrl(200, 200, 'food'));
            $produto->save();
        }
    }

    private function salvarFoto($nomeProduto, $urlFoto)
    {
        $client = new Client();
        $response = $client->get($urlFoto, ['connect_timeout' => 5, 'timeout' => 10]);
        if ($response->getStatusCode() !== 200) {
            throw new \Exception('Erro ao baixar a imagem');
        }
        $foto = (string) $response->getBody();
        $nomeArquivo = "{$nomeProduto}.jpg";
        Storage::disk('public')->put("produtos/{$nomeArquivo}", $foto);
        return "produtos/{$nomeArquivo}";
    }
}
