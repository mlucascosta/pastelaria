<?php

namespace Tests\Unit;

use App\Http\Controllers\ClienteController;
use App\Http\Requests\CreateClienteRequest;
use App\Models\Cliente;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Mockery;
use PHPUnit\Framework\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClienteControllerTest extends TestCase
{
    /**
     * @var ClienteController
     */
    private $clienteController;

    /**
     * @var Cliente|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private $clienteMock;

    use RefreshDatabase;


    public function setUp(): void
    {
        parent::setUp();
        $this->clienteMock = Mockery::mock(Cliente::class);
        $this->clienteController = new ClienteController($this->clienteMock);
    }

    public function testIndex()
    {
        $clientes = [
            new Cliente([
                'id' => 1,
                'nome' => 'Fulano',
                'email' => 'fulano@teste.com',
                'telefone' => '99999999',
                'data_nascimento' => Carbon::now()->subYears(30)->format('Y-m-d'),
                'endereco' => 'Rua A',
                'complemento' => '',
                'bairro' => 'Centro',
                'cep' => '99999-999',
            ]),
            new Cliente([
                'id' => 2,
                'nome' => 'Ciclano',
                'email' => 'ciclano@teste.com',
                'telefone' => '88888888',
                'data_nascimento' => Carbon::now()->subYears(25)->format('Y-m-d'),
                'endereco' => 'Rua B',
                'complemento' => '',
                'bairro' => 'Centro',
                'cep' => '88888-888',
            ]),
        ];

        $this->clienteMock->shouldReceive('all')->andReturn($clientes);

        $response = $this->clienteController->index();

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($clientes, $response->getData());
    }

    public function testShow()
    {
        $cliente = new Cliente([
            'id' => 1,
            'nome' => 'Fulano',
            'email' => 'fulano@teste.com',
            'telefone' => '99999999',
            'data_nascimento' => Carbon::now()->subYears(30)->format('Y-m-d'),
            'endereco' => 'Rua A',
            'complemento' => '',
            'bairro' => 'Centro',
            'cep' => '99999-999',
        ]);

        $this->clienteMock->shouldReceive('findOrFail')->with(1)->andReturn($cliente);

        $response = $this->clienteController->show(1);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($cliente, $response->getData());
    }

    public function testStore()
        {
        $request = new CreateClienteRequest([
            'nome' => 'Fulano',
            'email' => 'fulano@teste.com',
            'telefone' => '99999999',
            'data_nascimento' => Carbon::now()->subYears(30)->format('Y-m-d'),
            'endereco' => 'Rua A',
            'complemento' => '',
            'bairro' => 'Centro',
            'cep' => '99999-999',
        ]);

        $cliente = new Cliente();
        $cliente->fill($request->all());

        $this->clienteMock->shouldReceive('fill')->with($request->all())->andReturn($cliente);
        $this->clienteMock->shouldReceive('save')->andReturn(true);

        $response = $this->clienteController->store($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertEquals($cliente, $response->getData());
    }

    public function testUpdate()
    {
        $request = new CreateClienteRequest([
            'nome' => 'Fulano',
            'email' => 'fulano@teste.com',
            'telefone' => '99999999',
            'data_nascimento' => Carbon::now()->subYears(30)->format('Y-m-d'),
            'endereco' => 'Rua A',
            'complemento' => '',
            'bairro' => 'Centro',
            'cep' => '99999-999',
        ]);

        $cliente = new Cliente([
            'id' => 1,
            'nome' => 'Fulano',
            'email' => 'fulano@teste.com',
            'telefone' => '99999999',
            'data_nascimento' => Carbon::now()->subYears(30)->format('Y-m-d'),
            'endereco' => 'Rua A',
            'complemento' => '',
            'bairro' => 'Centro',
            'cep' => '99999-999',
        ]);

        $this->clienteMock->shouldReceive('findOrFail')->with(1)->andReturn($cliente);
        $this->clienteMock->shouldReceive('fill')->with($request->all())->andReturn($cliente);
        $this->clienteMock->shouldReceive('save')->andReturn(true);

        $response = $this->clienteController->update($request, 1);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($cliente, $response->getData());
    }

    public function testDelete()
    {
        $cliente = new Cliente([
            'id' => 1,
            'nome' => 'Fulano',
            'email' => 'fulano@teste.com',
            'telefone' => '99999999',
            'data_nascimento' => Carbon::now()->subYears(30)->format('Y-m-d'),
            'endereco' => 'Rua A',
            'complemento' => '',
            'bairro' => 'Centro',
            'cep' => '99999-999',
        ]);

        $this->clienteMock->shouldReceive('findOrFail')->with(1)->andReturn($cliente);
        $this->clienteMock->shouldReceive('delete')->andReturn(true);

        $response = $this->clienteController->delete(1);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals(['message' => 'Registro excluído com sucesso.'], $response->getData());
    }

    public function testDestroy()
    {
        $cliente = factory(Cliente::class)->create();

        $response = $this->delete(route('clientes.destroy', ['id' => $cliente->id]));

        $response->assertStatus(200);
        $this->assertDatabaseMissing('clientes', ['id' => $cliente->id]);

        $cliente = Cliente::withTrashed()->find($cliente->id);
        $this->assertNotNull($cliente->deleted_at);

        $response = $this->delete(route('clientes.destroy', ['id' => $cliente->id]));

        $response->assertStatus(200);
        $this->assertDatabaseMissing('clientes', ['id' => $cliente->id]);
    }

}