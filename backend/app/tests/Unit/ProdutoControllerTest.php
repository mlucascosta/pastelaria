<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Produto;
use App\Http\Controllers\ProdutoController;
use App\Http\Requests\CreateProdutoRequest;

class ProdutoControllerTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('public');
    }

    public function testIndex()
    {
        $this->withoutExceptionHandling();

        $produto = Produto::factory()->create();

        $controller = new ProdutoController;
        $response = $controller->index();

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'id' => $produto->id,
            'nome' => $produto->nome,
            'preco' => $produto->preco,
        ]);
    }

    public function testStore()
    {
        $this->withoutExceptionHandling();

        $nome = $this->faker->name();
        $preco = $this->faker->randomFloat(2, 0, 1000);

        $foto = UploadedFile::fake()->image('produto.jpg');

        $request = new CreateProdutoRequest([
            'nome' => $nome,
            'preco' => $preco,
            'foto' => $foto,
        ]);

        $controller = new ProdutoController;
        $response = $controller->store($request);

        $response->assertStatus(201);

        $produto = Produto::find($response->json('id'));

        Storage::disk('public')->assertExists($produto->foto);
        $this->assertEquals($nome, $produto->nome);
        $this->assertEquals($preco, $produto->preco);
    }

    public function testShow()
    {
        $this->withoutExceptionHandling();

        $produto = Produto::factory()->create();

        $controller = new ProdutoController;
        $response = $controller->show($produto->id);

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'id' => $produto->id,
            'nome' => $produto->nome,
            'preco' => $produto->preco,
        ]);
    }

    public function testUpdate()
    {
        $this->withoutExceptionHandling();

        $produto = Produto::factory()->create();

        $nome = $this->faker->name();
        $preco = $this->faker->randomFloat(2, 0, 1000);

        $foto = UploadedFile::fake()->image('produto.jpg');

        $request = new Request([
            'nome' => $nome,
            'preco' => $preco,
            'foto' => $foto,
        ]);

        $controller = new ProdutoController;
        $response = $controller->update($request, $produto->id);

        $response->assertStatus(200);

        $produto = Produto::find($produto->id);

        Storage::disk('public')->assertExists($produto->foto);
        $this->assertEquals($nome, $produto->nome);
        $this->assertEquals($preco, $produto->preco);
    }

    public function testDelete()
    {
        // Criar um novo produto
        $produto = factory(Produto::class)->create();

        // Excluir o produto criado
        $response = $this->json('DELETE', '/api/produtos/' . $produto->id);

        // Verificar se o status code é 200
        $response->assertStatus(200);

        // Verificar se a mensagem de sucesso é retornada
        $response->assertJson([
            'message' => 'Registro excluído com sucesso.'
        ]);

        // Verificar se o produto foi excluído do banco de dados
        $this->assertSoftDeleted('produtos', [
            'id' => $produto->id
        ]);
    }

    public function testDestroy()
    {
        $pedido = factory(Pedido::class)->create();
        $id = $pedido->id;

        $response = $this->json('DELETE', "/api/pedidos/{$id}/destroy");

        $response
            ->assertStatus(200)
            ->assertJson(['message' => 'Registro excluído com sucesso.']);

        $this->assertDatabaseMissing('pedidos', ['id' => $id]);
    }

}