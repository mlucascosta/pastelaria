<?php

namespace Tests\Unit;

use App\Http\Controllers\PedidoController;
use App\Http\Requests\CreatePedidoRequest;
use App\Http\Requests\PedidoItemRequest;
use App\Models\Pedido;
use App\Models\PedidoItem;
use Illuminate\Foundation\Testing\RefreshDatabase;  
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Mockery;
use Tests\TestCase;

class PedidoControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function testIndex()
    {        
        Pedido::factory()->count(10)->create();
        $controller = new PedidoController();
        $response = $controller->index();
        $this->assertEquals(10, count($response->original));
    }

    public function testShow()
    {
        $pedido = factory(Pedido::class)->create();
        $item1 = factory(PedidoItem::class)->create(['pedido_id' => $pedido->id]);
        $item2 = factory(PedidoItem::class)->create(['pedido_id' => $pedido->id]);
        $controller = new PedidoController();
        $response = $controller->show($pedido->id);
        $this->assertEquals(2, count($response->original));
    }

    public function testShowByCliente()
    {
        $cliente_id = 1;
        $pedido1 = factory(Pedido::class)->create(['cliente_id' => $cliente_id]);
        $pedido2 = factory(Pedido::class)->create(['cliente_id' => $cliente_id]);
        $item1 = factory(PedidoItem::class)->create(['pedido_id' => $pedido1->id]);
        $item2 = factory(PedidoItem::class)->create(['pedido_id' => $pedido1->id]);
        $item3 = factory(PedidoItem::class)->create(['pedido_id' => $pedido2->id]);
        $controller = new PedidoController();
        $response = $controller->showByCliente($cliente_id);
        $this->assertEquals(2, count($response->original[0]));
        $this->assertEquals(1, count($response->original[1]));
    }

    public function testStore()
    {
        Mail::fake();
        $cliente_id = 1;
        $item = factory(PedidoItem::class)->make();
        $request = new CreatePedidoRequest();
        $request->merge(['cliente_id' => $cliente_id, 'pedidoItem' => $item]);
        $controller = new PedidoController();
        $response = $controller->store($request);
        $this->assertEquals(201, $response->status());
        $this->assertDatabaseHas('pedidos', ['cliente_id' => $cliente_id]);
        $this->assertDatabaseHas('pedido_itens', ['pedido_id' => $response->original['id']]);
        Mail::assertSent(PedidoRealizado::class, function ($mail) use ($cliente_id) {
            return $mail->pedido->cliente_id === $cliente_id;
        });
    }

    public function testUpdate()
    {
        // cria um cliente e pedido fake
        $cliente = factory(App\Models\Cliente::class)->create();
        $pedido = factory(App\Models\Pedido::class)->create(['cliente_id' => $cliente->id]);

        // cria um item fake para o pedido
        $pedidoItem = factory(App\Models\PedidoItem::class)->create(['pedido_id' => $pedido->id]);

        // define os novos dados para atualização do pedido
        $novosDados = [
            'cliente_id' => $cliente->id,
            'pedidoItem' => [
                [
                    'produto_id' => $pedidoItem->produto_id,
                    'quantidade' => $this->faker->randomDigitNotNull,
                    'valor_unitario' => $this->faker->randomFloat(2, 1, 100)
                ]
            ]
        ];

        // faz a requisição PUT para atualizar o pedido
        $response = $this->json('PUT', '/pedidos/' . $pedido->id, $novosDados);

        // verifica se a resposta foi bem sucedida (200)
        $response->assertStatus(200);

        // verifica se os dados do pedido foram atualizados corretamente
        $this->assertDatabaseHas('pedidos', [
            'id' => $pedido->id,
            'cliente_id' => $cliente->id
        ]);

        // verifica se o item do pedido foi atualizado corretamente
        $this->assertDatabaseHas('pedido_items', [
            'pedido_id' => $pedido->id,
            'produto_id' => $pedidoItem->produto_id,
            'quantidade' => $novosDados['pedidoItem'][0]['quantidade'],
            'valor_unitario' => $novosDados['pedidoItem'][0]['valor_unitario']
        ]);

        // verifica se o e-mail de notificação foi enviado corretamente
        Mail::assertSent(PedidoRealizado::class, function ($mail) use ($pedido) {
            return $mail->pedido->id === $pedido->id;
        });
    }

    public function testDeletePedido()
    {
        $pedido = factory(Pedido::class)->create();

        $response = $this->delete('/api/pedidos/'.$pedido->id);

        $response->assertStatus(200);
        $this->assertDatabaseMissing('pedidos', ['id' => $pedido->id]);
    }

    public function testDestroyPedido()
    {
        $pedido = factory(Pedido::class)->create();
        $pedidoId = $pedido->id;

        $response = $this->delete('/api/pedidos/destroy/'.$pedidoId);

        $response->assertStatus(200);
        $this->assertDatabaseMissing('pedidos', ['id' => $pedidoId]);
        $this->assertDatabaseMissing('pedido_items', ['pedido_id' => $pedidoId]);
    }

}