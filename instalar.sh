#!/bin/bash

if ! docker info > /dev/null 2>&1; then
  echo "Este script usa o docker e não está em execução - inicie o docker e tente novamente!"
  exit 1
fi

echo 'rodando composer para gerar o docker do projeto'
docker-compose -f docker-compose.yml up -d

echo 'redefinindo permissões da pasta'
sudo chown -R $USER:$USER $(pwd)
cd backend/app
pwd
sudo chmod -R 777 storage/
docker exec pastelaria-backend php artisan key:generate

echo 'rodando as migrations do projeto e as seed'
docker exec pastelaria-backend php artisan migrate --seed

cd ..
cd ..
pwd